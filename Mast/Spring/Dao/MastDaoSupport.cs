﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mast.SpringDao
{
    public class MastDaoSupport
    {
        private MastTemplate mastTemplate;

        /// <summary>
        /// Set the AdoTemplate for this DAO explicity, as
        /// an alternative to specifying a IDbProvider
        /// </summary>
        public MastTemplate MastTemplate
        {
            set
            {
                mastTemplate = value;
            }
            get
            {
                return mastTemplate;
            }

        }
    }
}