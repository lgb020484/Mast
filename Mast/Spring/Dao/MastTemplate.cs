#region License

/*
 * Copyright ?2002-2007 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#endregion

#region Imports

using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Reflection;
using Common.Logging;
using Mast.DBUtility;
using Mast.Common;
using Spring;
using Spring.Data;
using Spring.Data.Support;
using Spring.Data.Common;
using Mast.Session;
using System.Collections.Generic;

#endregion

namespace Mast.SpringDao
{
    /// <summary>
    /// This is the central class in the Spring.Data namespace.
    /// It simplifies the use of ADO.NET and helps to avoid commons errors.
    /// </summary>
    /// <author>Mark Pollack (.NET)</author>
    public class MastTemplate
    {
        #region Logging Definition

        private static readonly ILog LOG = LogManager.GetLogger(typeof(MastTemplate));

        #endregion

        #region Fields

        private IDbProvider dbProvider;

        private string DbType { get; set; }

        private DbProviderProxy providerProxy;

        private IAdoExceptionTranslator exceptionTranslator;


        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MastTemplate"/> class.
        /// </summary>
        public MastTemplate()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MastTemplate"/> class.
        /// </summary>
        /// <param name="provider">The database provider.</param>
        public MastTemplate(IDbProvider provider)
        {
            DbProvider = provider;
            AfterPropertiesSet();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MastTemplate"/> class.
        /// </summary>
        /// <param name="provider">The database provider.</param>
        /// <param name="lazyInit">if set to <c>false</c>
        /// lazily initialize the ErrorCodeExceptionTranslator.</param>
        public MastTemplate(IDbProvider provider, bool lazyInit)
        {
            DbProvider = provider;
            AfterPropertiesSet();
        }

        #endregion

        #region Properties

        /// <summary>
        /// An instance of a DbProvider implementation.
        /// </summary>
        public IDbProvider DbProvider
        {
            get { return dbProvider; }
            set
            {
                dbProvider = value;
                DbProviderProxy.Instance.SetProvider(dbProvider);
            }
        }
        #endregion

        public DbProviderProxy ProviderProxy
        {
            get 
            {
                if (providerProxy != null) return providerProxy;
                DbProviderProxy.Instance.SetProvider(dbProvider);

                return DbProviderProxy.Instance; 
            }
            set
            {
                providerProxy = value;
            }
        }


        /// <summary>
        /// Gets or sets the exception translator. If no custom translator is provided, a default
        /// <see cref="ErrorCodeExceptionTranslator"/> is used.
        /// </summary>
        /// <value>The exception translator.</value>
        public IAdoExceptionTranslator ExceptionTranslator
        {
            get
            {
                InitExceptionTranslator();
                return exceptionTranslator;
            }
            set
            {
                exceptionTranslator = value;
            }
        }

        public void AfterPropertiesSet()
        {
            if (DbProvider == null)
            {
                throw new ArgumentException("DbProvider is required");
            }
            
            InitExceptionTranslator();
        }

        protected void InitExceptionTranslator()
        {
            if (exceptionTranslator == null)
            {
                if (DbProvider != null)
                {
                    exceptionTranslator = new ErrorCodeExceptionTranslator(DbProvider);
                }
                else
                {
                    exceptionTranslator = new FallbackExceptionTranslator();
                }
            }
        }

        #region 将实体数据保存到数据库
        public int Insert<T>(T entity)
        {
            Session.Session session = SessionFactory.GetSession();
            return session.Insert(entity);
        }
        #endregion

        #region 批量保存
        public int Insert<T>(List<T> entityList)
        {
            Session.Session session = SessionFactory.GetSession();
            return session.Insert(entityList);
        }
        #endregion

        #region 将实体数据修改到数据库
        public int Update<T>(T entity)
        {
            Session.Session session = SessionFactory.GetSession();
            return session.Update(entity);
        }
        #endregion

        #region 批量更新
        public int Update<T>(List<T> entityList)
        {
            Session.Session session = SessionFactory.GetSession();
            return session.Update(entityList);
        }
        #endregion

        #region 将实体数据修改到数据库
        public int ExcuteSQL(string strSQL, ParamMap param)
        {
            Session.Session session = SessionFactory.GetSession();
            return session.ExcuteSQL(strSQL, param);
        }
        #endregion

        #region 删除实体对应数据库中的数据
        public int Delete<T>(T entity)
        {
            Session.Session session = SessionFactory.GetSession();
            return session.Delete<T>(entity);
        }
        #endregion

        #region 批量删除
        public int Delete<T>(List<T> entityList)
        {
            Session.Session session = SessionFactory.GetSession();
            return session.Delete<T>(entityList);
        }
        #endregion

        #region 根据主键id删除实体对应数据库中的数据
        public int Delete<T>(object id) where T : new()
        {
            Session.Session session = SessionFactory.GetSession();
            return session.Delete<T>(id);
        }
        #endregion

        #region 批量根据主键id删除数据
        public int Delete<T>(object[] ids) where T : new()
        {
            Session.Session session = SessionFactory.GetSession();
            return session.Delete<T>(ids);
        }
        #endregion

        #region 通过自定义SQL语句查询记录数
        public int Count(string strSQL)
        {
            Session.Session session = SessionFactory.GetSession();
            return session.Count(strSQL);
        }
        #endregion

        #region 通过自定义SQL语句查询记录数
        public int Count(string strSQL, ParamMap param)
        {
            Session.Session session = SessionFactory.GetSession();
            return session.Count(strSQL, param);
        }
        #endregion

        #region 通过自定义SQL语句查询数据
        public List<T> Find<T>(string strSQL) where T : new()
        {
            Session.Session session = SessionFactory.GetSession();
            return session.Find<T>(strSQL);
        }
        #endregion

        #region 通过自定义SQL语句查询数据
        public List<T> Find<T>(string strSQL, ParamMap param) where T : new()
        {
            Session.Session session = SessionFactory.GetSession();
            return session.Find<T>(strSQL, param);
        }
        #endregion

        #region 分页查询返回分页结果
        public PageResult<T> FindPage<T>(string strSQL, ParamMap param) where T : new()
        {
            Session.Session session = SessionFactory.GetSession();
            return session.FindPage<T>(strSQL, param);
        }
        #endregion

        #region 通过主键ID查询数据
        public T Get<T>(object id) where T : new()
        {
            Session.Session session = SessionFactory.GetSession();
            return session.Get<T>(id);
        }
        #endregion


    }
}